#include "car.h"

//Initialize the car to default and waits that the contact key is turned on, then returns
void initCar()
{
    gear = 0;       //Reset differents variables to zero
    accelPedal = 0;
    brakePedal = 0;
    rpm = 0;
    speed = 0;
    speedGoal = 0;
    tempomat = 0;
    K_term = 0;
    I_term = 0;
    
    sendCar(CAR_RST,0,0);   //Reset the car
    
    //Turn off the lights (front/back)
    uint8_t data = 0;
    sendCar(LIGHT_FRONT,1,&data);
    sendCar(LIGHT_BACK,1,&data);
    
    //wait until the key is turned on
    do
    {
        waitForResponse(&msg);
    }while(msg.identifier != (CONTACT_KEY | ourCarID) || msg.dta[0] != 1);
    
    changeMode(MODE_P);     //Change to park mode
}

//Initialize the microcontroller (ports, timers, peripherals,...) for our application
void initUC()
{
    EECON2 = 0x55;              //To be able to write on RTCWREN
    EECON2 = 0xAA;
    RTCCFGbits.RTCWREN = 1;     //Enables the write operation on RTCC registers
    RTCEN = 1;                  //Enables RTC module
    //CONFIG3L.RTCOSC = 1;      //32768kHz clock
    RTCCIE = 1;                 //Enables RTC interruptions
    RTCOE = 0;                  //No output
    ALRMEN = 1;                 //Enables alarm on RTCC
    ALRMCFGbits.AMASK = 0b0001; //Configure AMASK for an alarm every second
    ALRMCFGbits.CHIME = 1;      //Enable periodic alarm
    
    RTCCFGbits.RTCPTR = 1;      //Setup hours and minutes by default to 14h39
    RTCVALL = 0x14;
    RTCCFGbits.RTCPTR = 0;
    RTCVALH = 0x39;
    
    EECON2 = 0x55;              //To be able to write on RTCWREN
    EECON2 = 0xAA;
    RTCCFGbits.RTCWREN = 0;     //Disables the write operation on RTCC registers
            
            
    TMR1IE = 1;             //Enables the timer1 interrupt
    TMR1IF = 0;
    T1CONbits.SOSCEN = 1;   //external clock with 32768Hz
    T1CONbits.TMR1CS0 = 0;
    T1CONbits.TMR1CS1 = 1;  
    T1CONbits.T1CKPS = 0;
    T1CONbits.RD16 = 1;     //16 bit operation 
    TMR1 = 62259;           //TMR value for 100ms
    TMR1ON = 0;             //Disables the timer1   (PI Regulation for the tempomat)
    
    TMR3IE = 1;             //Enables the timer3 interrupt
    TMR3IF = 0;
    T3CONbits.SOSCEN = 1;   //external clock with 32768Hz
    T3CONbits.TMR3CS0 = 0;
    T3CONbits.TMR3CS1 = 1;
    T3CONbits.T3CKPS = 0;
    T3CONbits.RD16 = 1;     //16 bit operation 
    TMR3 = 62259;           //TMR value for 100ms
    TMR3ON = 0;             //Disables the timer3   (RPM, speed and front sensor limitations)
    
    TMR5IE = 1;             //Enables the timer5 interrupt
    TMR5IF = 0;
    T5CONbits.SOSCEN = 1;   //external clock with 32768Hz
    T5CONbits.TMR5CS0 = 0;
    T5CONbits.TMR5CS1 = 1;
    T5CONbits.T5CKPS = 0;
    T5CONbits.RD16 = 1;     //16 bit operation 
    TMR5 = 62259;           //TMR value for 100ms
    TMR5ON = 1;             //Enables the timer5    (meters count to send a pulse to the car every 100m)
    
    //Enable interrupts
    GIEL = 1;   //Peripheral
    GIE = 1;    //Global
}

//Wait for a message to come and returns. The message is saved in the given CANMESSAGE
void waitForResponse(struct CANMESSAGE *message)
{
    uint8_t response;
    do
    {
        response = Can_GetMessage(message);
    }
    while(response!=CAN_OK);
}

//Change the car to the given mode
void changeMode(char mode)
{
    currentMode = mode;             //Switch to the mode
    TMR1ON = 0;                     //Resets different variables
    frontSensor = 0;
    frontSensorLimitation = false;
    uint8_t frontLights = 0;
    uint8_t backLights = 0;
    uint8_t brake = 0;
    uint8_t motorPower = 0;
    switch(mode)                    //In function of the mode, set the front/back lights, gear, brake and motor power
    {
        case MODE_P:
            //Frontlights 50%
            frontLights = 50;
            //Brakelights
            backLights = 100;
            //Gear 0
            gear = 0;
            //Brake 100%
            brake = 100;
            //Motor power ~ pedal
            motorPower = pwrMotorWithAccPedal(accelPedal);
            //Disables limitations
            TMR3ON = 0;             //Disables the timer3
            break;
        case MODE_N:
            //Frontlights 50%
            frontLights = 50;
            //Backlights low beam (pedal)
            if(brakePedal>PEDAL_DEAD_BAND)
            {
                backLights = 100;
            }
            else
            {
                backLights = 50;
            }
            //Gear 0
            gear = 0;
            //Brake 0% (pedal)
            brake = brakePedal;
            //Motor power ~ pedal
            motorPower = pwrMotorWithAccPedal(accelPedal);
            //Disables limitations
            TMR3ON = 0;             //Disables the timer3
            break;
        case MODE_R:
            //Frontlights 50%
            frontLights = 50;
            //Backlights low beam (pedal)
            if(brakePedal>PEDAL_DEAD_BAND)
            {
                backLights = 100;
            }
            else
            {
                backLights = 50;
            }
            //Gear 0 (-> 1)
            gear = 0;
            //Brake 0% (pedal)
            brake = brakePedal;
            //Motor power ~ pedal
            motorPower = pwrMotorWithAccPedal(accelPedal);
            //Enables limitations
            TMR3ON = 1;             //Enables the timer3
            break;
        case MODE_D:
            //Frontights 100%
            frontLights = 100;
            //Backlights low beam (pedal)
            if(brakePedal>PEDAL_DEAD_BAND)
            {
                backLights = 100;
            }
            else
            {
                backLights = 50;
            }
            //Gear 0(->5)
            gear = 0;
            //Brake 0% (pedal)
            brake = brakePedal;
            //Motor power ~ pedal
            motorPower = pwrMotorWithAccPedal(accelPedal);
            //Enables limitations
            TMR3ON = 1;             //Enables the timer3
            break;
        case MODE_OFF:
            //Frontights OFF
            frontLights = 0;
            //Backlights OFF
            backLights = 0;
            //Gear 0
            gear = 0;
            //Brake OFF
            brake = 0;
            //Motor power OFF
            motorPower = 0;
            //Disables limitations
            TMR3ON = 0;             //Disables the timer3
            break;
        default:break;
    }
    //Frontlights
    sendCar(LIGHT_FRONT,1,&frontLights);
    //Backlights
    sendCar(LIGHT_BACK,1,&backLights);
    //Gear
    sendCar(GEAR_LVL,1,&gear);
    //Brake
    sendCar(PWR_BRAKE,1,&brake);
    //TEMPO OFF
    sendCar(TEMPO_OFF,0,0);             //Always TEMPO_OFF
    //Motor power
    sendCar(PWR_MOTOR,1,&motorPower);
}

//Send to our car the given message
void sendCar(uint16_t idToSend, uint8_t dataLength, uint8_t* data)
{
    if(dataLength<=8)       //Only if the datas are smaller or equal as 8 bytes
    {
        uint8_t i = 0;
        msg.extended_identifier = 0;            //No extended identifier
        msg.identifier = idToSend | ourCarID;   //Sets the identifier for the given ID and our car ID
        msg.dlc = dataLength;                   //Sets the data length
        for(i=0;i!=dataLength;i++)
        {
            msg.dta[i] = data[i];               //Fills the message data with the given data
        }
        msg.rtr = 0;                            //No response
        msg.filhit = 0;                         //Filter 0
        msg.txPrio = 3;                         //Maximum priority
        Can_PutMessage(&msg);                   //Sends the configured message
    }
}

//Send a request to the car and doesn't wait for an answer
void askCar(uint16_t idToAsk)
{
        msg.extended_identifier = 0;            //No extended identifier
        msg.identifier = idToAsk | ourCarID;    //Sets the identifier for the given ID and our car ID
        msg.dlc = 0;                            //No data sent
        msg.rtr = 1;                            //Request for a response
        msg.filhit = 0;                         //Filter 0
        msg.txPrio = 3;                         //Maximum priority
        Can_PutMessage(&msg);                   //Sends the configured message
}

//Send a request to the car and wait for the car to respond, then returns
void requestCar(uint16_t idToRequest)
{
    askCar(idToRequest);                        //Ask the car about the given ID
    
    //wait until response
    do
    {
        waitForResponse(&msg);                  //Wait until a response for our request is detected
    }while(msg.identifier != (idToRequest | ourCarID));
}

//Returns the calculated rpm to give to the motor in function of the acceleration pedal
uint8_t pwrMotorWithAccPedal(uint8_t pedal)
{
    return pedal*(LIMIT_RPM_UP-LIMIT_RPM_DOWN)/100 + LIMIT_RPM_DOWN;    //The returned value is mapped between LIMIT_RPM_DOWN rpm and LIMIT_RPM_UP rpm
}