#ifndef CAR_H_
#define CAR_H_
#include "can.h"
#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

// Define from car to controller

#define TEMPOMAT 		(0x10)
#define GEAR_SEL 		(0x20)
#define FRONT_SENS_REQ 	(0x30)
#define EXT_SENSORS 	(0x30)
#define MOTOR_STATUS 	(0x40)
#define BRAKE_PEDAL 	(0x60)
#define ACCEL_PEDAL 	(0x70)
#define CONTACT_KEY 	(0x80)
#define STEERING_W_REQ 	(0x90)
#define BROKEN_CAR 		(0xA0)
#define BAD_MSG 		(0xB0)
#define SLOPE_REQ 		(0xC0)
#define RACE 			(0xD0)
#define CAR_ID 			(0xFF)


// Define from controller to car

#define LIGHT_FRONT 	(0x110)
#define LIGHT_BACK 		(0x120)
#define TIME 			(0x130)
#define GEAR_LVL 		(0x140)
#define AUDIO 			(0x150)
#define PWR_MOTOR 		(0x160)
#define PWR_BRAKE 		(0x170)
#define TEMPO_OFF 		(0x180)
#define KM_PULSE 		(0x190)
#define AUTO_STEERING 	(0x1A0)
#define CAR_RST 		(0x1F0)

// Define mode possibilities

#define MODE_OFF 0
#define MODE_P 'P'
#define MODE_N 'N'
#define MODE_R 'R'
#define MODE_D 'D'

// Define limits
#define LIMIT_RPM_UP 75
#define LIMIT_RPM_DOWN 10
#define LIMIT_RPM_GEAR_UP 5000
#define LIMIT_RPM_GEAR_DOWN 2000
#define LIMIT_SPEED 190

// Other defines
#define PEDAL_DEAD_BAND 5
#define SENSOR_TRIGGER 2048
#define MAX_GEAR_NUMBER 5
#define TEMPOMAT_GEAR_0_RPM 30

// Global variables
struct CANMESSAGE msg;
uint8_t returnCode;
uint8_t ourCarID;
uint8_t column = 0;
uint8_t frontSensorLimitation = false;

// Variables telling the states of the car
char currentMode = MODE_OFF;
uint8_t gear = 0;
uint8_t accelPedal = 0;
uint8_t brakePedal = 0;
uint16_t rpm = 0;
int16_t speed = 0;
uint16_t frontSensor = 0;

double metersCount = 0;

// Variables for PI regulation for tempomat
uint8_t speedGoal = 0;
int16_t tempomat = 0;
const uint8_t Kp = 40;
double K_term = 0;
const uint8_t Ki = 1;
double I_term = 0;
const double IMax = 300.0;

// Function declaration

//Initialize the car to default and waits that the contact key is turned on, then returns
void initCar();
//Initialize the microcontroller (ports, timers, peripherals,...) for our application
void initUC();
//Wait for a message to come and returns. The message is saved in the given CANMESSAGE
void waitForResponse(struct CANMESSAGE *message);
//Change the car to the given mode
void changeMode(char mode);
//Send to our car the given message
void sendCar(uint16_t idToSend, uint8_t dataLength, uint8_t* data);
//Send a request to the car and doesn't wait for an answer
void askCar(uint16_t idToAsk);
//Send a request to the car and wait for the car to respond, then returns
void requestCar(uint16_t idToRequest);
//Returns the calculated rpm to give to the motor in function of the acceleration pedal
uint8_t pwrMotorWithAccPedal(uint8_t pedal);
#endif