/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif
#define _XTAL_FREQ 64000000L

#include "picebs2_canlib/can.h"
#include "picebs2_lcdlib/lcd_highlevel.h"

#include "picebs2_canlib/car.h"

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

void main(void)
{
    /* Configure the oscillator for the device works @ 64MHz*/
   PLLEN = 1;            // activate PLL x4
   OSCCON = 0b01110000;  // for 64MHz cpu clock (default is 8MHz)
    // Caution -> the PLL needs up to 2 [ms] to start !
    __delay_ms(2); 
    /* Initialize I/O and Peripherals for application */

    //Enable interrupts
    GIE = 1;
    
    struct CANFILTER filter;        //Configure CAN filters and masks to obtain only 0xFF messages (CAR_ID)
    filter.mask0 = 0xFF;
    filter.mask1 = 0xFF;
    filter.filter0 = 0xFF;
    filter.filter1 = 0xFF;
    filter.filter2 = 0xFF;
    filter.filter3 = 0xFF;
    filter.filter4 = 0xFF;
    filter.filter5 = 0xFF;
    filter.ext0 = 0;
    filter.ext1 = 0;
    
    Can_Init(&filter);
    
    requestCar(CAR_ID);             //Request the car to give back it's ID
    
    ourCarID = msg.dta[0];          //Retrieve the car ID from the message
    
    filter.mask0 = 0x00F;           //Configure CAN filters and masks to obtain messages from our car only
    filter.mask1 = 0x00F;
    filter.filter0 = ourCarID;
    filter.filter1 = ourCarID;
    filter.filter2 = ourCarID;
    filter.filter3 = ourCarID;
    filter.filter4 = ourCarID;
    filter.filter5 = ourCarID;
    filter.ext0 = 0;
    filter.ext1 = 0;
    Can_Init(&filter);
    
    initUC();                       //Initialize ports and timers for our application
    initCar();                      //Initialise the car
    
    while(1)
    {
        if(Can_GetMessage(&msg)==CAN_OK)
        {
            switch(msg.identifier-ourCarID)             //Only do something when a message is received, do something different in function of the identifier
            {
                case TEMPOMAT:                          //TEMPOMAT : only in drive mode, activates the timer 1 responsible for the regulation and retrieve the speed goal value
                    switch(currentMode)
                    {
                        case MODE_D:
                            if(TMR1ON == 0 || msg.dta[0] == 0)
                            {
                                tempomat = pwrMotorWithAccPedal(accelPedal);
                            }
                            I_term = 0;
                            TMR1ON = msg.dta[0];
                            speedGoal = msg.dta[1];
                            sendCar(PWR_MOTOR,1,&tempomat);
                            break;
                        default:                        //In other modes, send back TEMPO_OFF
                            sendCar(TEMPO_OFF,0,0);
                            break;
                    }
                    break;
                case GEAR_SEL:                          //GEAR_SEL : Change mode in function of the selected 
                    changeMode(msg.dta[0]);
                    break;
                case FRONT_SENS_REQ | EXT_SENSORS:      //FRONT_SENS_REQ : Retrieve the asked front sensor values
                    frontSensor = (uint16_t)(msg.dta[0]<<8);
                    frontSensor += msg.dta[1];
                    break;
                case MOTOR_STATUS:                      //MOTOR_STATUS : Retrieve the current rpm and speed of our car
                    rpm = (uint16_t)(msg.dta[0]<<8);
                    rpm += msg.dta[1];
                    speed = (int16_t)(msg.dta[2]<<8);
                    speed += msg.dta[3];
                    switch(currentMode)
                    {
                        case MODE_D:                            //In drive mode : Switch to higher/lower gear when needed
                            if(gear!=0)
                            {
                                if(rpm>LIMIT_RPM_GEAR_UP)       //Greater than LIMIT_RPM_GEAR_UP rpm
                                {
                                    if(gear<MAX_GEAR_NUMBER)    //If not on the maximum gear
                                    {
                                        gear++;                 //Switch to higher gear
                                        sendCar(GEAR_LVL,1,&gear);
                                    }
                                }
                                else if(rpm<LIMIT_RPM_GEAR_DOWN) //Lower than LIMIT_RPM_GEAR_DOWN rpm
                                {
                                    gear--;                     //Switch to lower gear
                                    sendCar(GEAR_LVL,1,&gear);
                                }
                            }
                            else
                            {
                                if(rpm>LIMIT_RPM_GEAR_DOWN && !frontSensorLimitation)   //Do not set the gear to 1 if the front sensors are active
                                {
                                    gear = 1;                   //Switch gear to one
                                    sendCar(GEAR_LVL,1,&gear);
                                }
                            }
                            break;
                        case MODE_R:                            //In reverse mode, either gear 1 or 0
                            if(accelPedal>PEDAL_DEAD_BAND)
                            {
                                if(gear!=1)                     //Gear = 1 when the pedal is pressed
                                {
                                    gear = 1;
                                    sendCar(GEAR_LVL,1,&gear);
                                }
                            }
                            else                                //Gear = 0 when the pedal is not pressed
                            {
                                if(gear!=0)
                                {
                                    gear = 0;
                                    sendCar(GEAR_LVL,1,&gear);
                                }
                            }
                            break;
                        default:break;
                    }
                    break;
                case BRAKE_PEDAL:                               //BRAKE_PEDAL : Retrieve the position of the pedal
                    brakePedal = msg.dta[0];
                    if(currentMode!=MODE_P)                     //If not in Park mode :
                    {
                        uint8_t intensity;
                        if(brakePedal>PEDAL_DEAD_BAND)          //if the pedal is pressed, brake lights
                        {
                            intensity = 100;
                            sendCar(LIGHT_BACK,1,&intensity);
                        }
                        else                                    //else low beam backlights
                        {
                            intensity = 50;
                            sendCar(LIGHT_BACK,1,&intensity);
                        }
                        sendCar(PWR_BRAKE,1,&brakePedal);       //brake the car in function of the pedal
                    }
                    sendCar(TEMPO_OFF,0,0);
                    break;
                case ACCEL_PEDAL:                               //ACCEL_PEDAL : Retrieve the position of the pedal
                    accelPedal = msg.dta[0];
                    uint8_t data = pwrMotorWithAccPedal(accelPedal);
                    sendCar(PWR_MOTOR,1,&data);                 //power the motor in function of the pedal
                    break;
                case CONTACT_KEY:                               //CONTACT_KEY : The contact key is always on when we come here 
                    changeMode(MODE_OFF);                       //=> contact key just switched off -> change mode to off
                    break;
                case STEERING_W_REQ:    //STEERING_W_REQ : not used
                    break;
                case BROKEN_CAR:        //BROKEN_CAR :
                    __delay_ms(2000);   //wait 2 seconds and reset the car
                    changeMode(MODE_OFF);
                    break;
                case BAD_MSG:       //BAD_MSG : not used
                    break;
                case SLOPE_REQ:     //SLOPE_REQ : not used
                    break;
                case RACE:          //RACE : not used
                    break;
                case CAR_ID:        //CAR_ID : not used
                    break;
                default:break;
            }
        }
        if(currentMode == MODE_OFF) //If we just switched to mode off
        {
            initCar();              //Re-init the car and wait the contact key is turned back on
        }
    }
}
