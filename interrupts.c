/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include "picebs2_canlib/can.h"
#include "picebs2_canlib/car.h"
#endif

/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/
/* High-priority service */

void interrupt high_isr(void)
{
    //interrupt on CAN character received
    if((CAN_INTF == 1)&&(CAN_INTE == 1)) // interrupt flag & active
    {
        CAN_INTF = 0;               // clear interrupt
        Can_Isr();                  // interrupt treatment
        if(CAN_INTPIN == 0)         // check pin is high again
        {
          CAN_INTF = 1;             // no -> re-create interrupt
        }
    }
    //interrupt on timer1 overflow
    //PI Regulation for the tempomat
    else if(TMR1IF == 1 && TMR1IE == 1){
        TMR1IF = 0;             //Disable interrupt flag
        TMR1 = 62259;           //TMR value for 100ms
        
        int16_t err = speedGoal-speed;          //Retrieve the error
        if(gear != 0)                           //If the gear is not on 0 (neutral)
        {
            K_term = Kp*err;                //Calculated proportional term
            I_term += (double)(err*Ki);     //Calculated integral term
            if(I_term > IMax)               //Limits the integral term
            {
                I_term = IMax;
            }
            else if(I_term < -IMax)
            {
                I_term = -IMax;
            }
            
            //Calculates the output command (Divided by gear to keep it continuous)
            tempomat = (int16_t)((double)(K_term/gear) + (double)(I_term/gear));
            
            if(tempomat<LIMIT_RPM_DOWN)     //Limits the given rpm value between LIMIT_RPM_DOWN rpm and LIMIT_RPM_UP rpm
            {
                tempomat = LIMIT_RPM_DOWN;
            }
            else if(tempomat>LIMIT_RPM_UP)
            {
                tempomat = LIMIT_RPM_UP;
            }
        }
        else    //if the gear is at 0 (neutral)
        {
            tempomat = TEMPOMAT_GEAR_0_RPM; //Give the motor a fix command to switch to gear 1
        }
        sendCar(PWR_MOTOR,1,&tempomat);     //Send the calculated command to the motor
    }
    //interrupt on timer3 overflow
    //RPM, speed and front sensor limitations
    else if(TMR3IF == 1 && TMR3IE == 1){
        TMR3IF = 0;             //Disable interrupt flag
        TMR3 = 62259;           //TMR value for 100ms
        
        uint8_t limitation = false;     //Initially set to no limitation
        frontSensorLimitation = false;
        uint8_t intensity = 0;
        if(rpm>(LIMIT_RPM_UP*100))  //RPM limitation
        {
            intensity = 50;         //brake 50%
            limitation = true;      //Activates the limitation
        }
        if(speed>LIMIT_SPEED)       //Speed limitation
        {
            intensity = 100;        //brake 100%
            limitation = true;      //Activates the limitation
        }
        if(currentMode == MODE_D)   //Only in drive mode
        {
            askCar(FRONT_SENS_REQ); //Ask our car about the front sensor (The values sent by the car are retrieved later in the main function)
            if(frontSensor>SENSOR_TRIGGER)      //If sensor is under a set limit
            {
                if(gear!=0)                     //Set the gear to 0 and disables the tempomat if that isn't already the case
                {
                    gear = 0;
                    sendCar(GEAR_LVL,1,&gear);
                    sendCar(TEMPO_OFF,0,0);
                }
                intensity = 100;                //brake 100%
                frontSensorLimitation = true;   //Disable the gear switch from 0 to 1
                limitation = true;              //Activates the limitation
            }
        }
        if(limitation)      //if the limitation needs to be applied
        {
            sendCar(PWR_BRAKE,1,&intensity);               //Send brake comand
            intensity = 100;
            sendCar(LIGHT_BACK,1,&intensity);              //Brakelights
        }
        else                //if no limitaion is needed
        {
            if(brakePedal>PEDAL_DEAD_BAND)  //brake pedal is pressed    //BRAKE FROM PEDAL   
            {
                intensity = 100;
                sendCar(LIGHT_BACK,1,&intensity);   //Brakelights
            }
            else                            //brake pedal is not pressed
            {
                intensity = 50;
                sendCar(LIGHT_BACK,1,&intensity);   //Low beam backlights
            }
            sendCar(PWR_BRAKE,1,&brakePedal);       //Brake ~ pedal
        }
    }
    //interrupt on timer5 overflow
    //Counting the distance to send a pulse to the car every 100m
    else if(TMR5IF == 1 && TMR5IE == 1){
        TMR5IF = 0;             //Disable interrupt flag
        TMR5 = 62259;           //TMR value for 100ms
        
        if(speed>=0)    //The meters count always goes up, even when the car is in reverse
        {
            metersCount += (double)(speed/36.0);
        }
        else
        {
            metersCount -= (double)(speed/36.0);
        }
        if(metersCount>100)         //Every 100m :
        {
            metersCount -= 100;     //Reduces the count by 100m
            sendCar(KM_PULSE,0,0);  //Send a pulse to the car
        }
    }
    //Interrupt from the RTC every second
    else if(RTCCIF == 1 && RTCCIE == 1)
    {
        uint8_t data[3];
        RTCCIF = 0;             //Disable the interrupt flag
        RTCCFGbits.RTCPTR = 1;  //Set the RTC pointer to one => RTCVALH = WEEKDAY and RTCVALL = HOURS
        data[0] = RTCVALL;      //Retrieve the current hours
        data[0] = ((data[0]&0xF0)>>4)*10 + (data[0]&0x0F); //Translates from BCD to binary
        RTCCFGbits.RTCPTR = 0;  //Set the RTC pointer to zero => RTCVALH = MINUTES and RTCVALL = SECONDS
        data[1] = RTCVALH;      //Retrieve the current minutes
        data[1] = ((data[1]&0xF0)>>4)*10 + (data[1]&0x0F); //Translates from BCD to binary
        data[2] = column;       //Set the column state
        column = 1-column;      //Invert the column state for the next second
        sendCar(TIME,3,&data);  //Send the current time to our car
    }
}
