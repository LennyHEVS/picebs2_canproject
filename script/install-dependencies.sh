#!/bin/bash

# A simple script to install required dependencies

# Keep track of where we are
# TODO!!

# Make a temp dir for install downloads
mkdir ../install-dependencies
cd ../install-dependencies

# Install Microchip MPLAB XC8 compiler (V1.45)
echo "==========================================================="
echo "- Downloading Microchip XC8 Compiler v1.45"
wget --no-verbose http://ww1.microchip.com/downloads/en/DeviceDoc/xc8-v1.45-full-install-linux-installer.run
echo "- Download finished..."
echo "- Adding excecution rights..."
chmod +x xc8-v1.45-full-install-linux-installer.run
echo "- Installing Microchip XC8 Compiler"
sudo ./xc8-v1.45-full-install-linux-installer.run --mode unattended --netservername dontknow
echo "- Installation of Microchip XC8 Compiler finished."
echo "==========================================================="
echo

# Install Microchip MPLAB X IDE (V5.35)
echo "==========================================================="
echo "- Downloading Microchip MPLAD X IDE"
wget --no-verbose http://ww1.microchip.com/downloads/en/DeviceDoc/MPLABX-v5.35-linux-installer.tar
echo "- Download finished..."
echo "- Unpacking tarball..."
tar -xvf MPLABX-v5.35-linux-installer.tar
echo "- Unpack finished..."
echo "- Installing MPLAB X IDE..."
sudo ./MPLABX-v5.35-linux-installer.sh -- --mode unattended
echo "- Installation of MPLAB X IDE finished."
echo "==========================================================="
echo
